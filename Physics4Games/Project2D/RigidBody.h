#pragma once
#include "PhysicsObject.h"
#include <functional>

class Rigidbody : public PhysicsObject {
public:
	Rigidbody(ShapeType shapeID, glm::vec2 position,
		glm::vec2 velocity, float orientation, float mass);
	~Rigidbody();

	virtual void fixedUpdate(glm::vec2 gravity, float timeStep);
	void applyForce(glm::vec2 force, glm::vec2 pos);

	void resolveCollision(Rigidbody* actor2, glm::vec2 contact, glm::vec2* collisionNormal = nullptr, float pen = 0);

	float getKineticEnergy();
	float getPotentialEnergy();
	float getEnergy();

	void resetPosition();
	void drawDebug();

	void setPosition(glm::vec2 pos) { m_position = pos; }
	glm::vec2 toWorld(glm::vec2 localPos);

	glm::vec2 rotateAroundPoint(glm::vec2 localPos, float angle);

	float getOrientatation() { return m_orientation; }
	glm::vec2 getVelocity() { return m_velocity; }
	float getAngularVelocity() { return m_angularVelocity; }
	float getMass() { return m_isKinematic ? INT_MAX : m_mass; }
	float getMoment() { return m_isKinematic ? INT_MAX : m_moment; }
	float getElasticity() { return m_elasticity; }
	bool isKinematic() { return m_isKinematic; }

	std::function<void(PhysicsObject*)> collisionCallback;

	bool m_isKinematic; 

protected:
	//inital variables to keep 
	glm::vec2 m_initalPosition;
	glm::vec2 m_initalVelocity;
	float m_initalOrientation;
	float m_initalAngularVelocity;

	glm::vec2 m_velocity;
	float m_angularVelocity;
	glm::vec2 m_savedGravity; //allows relative gravity? may be able to remove after verifying effectiveness
	float m_mass;
	float m_orientation; //2D so we only need a single float to represent our orientation 
	float m_moment;

	float m_elasticity = 0.3f;

	float m_linearDrag = 0.01;
	float m_angularDrag = 1.1;

	const float MIN_LINEAR_THRESHOLD = 0.001;
	const float MIN_ANGULAR_THRESHOLD = 0.001;
};