#include "Terrain.h"
#include <Gizmos.h>

Terrain::Terrain(glm::vec2 position, float orientation, Terrain* previous, glm::vec2 size, glm::vec4 colour) :
	Box(position, { 0,0 }, 0, orientation, size, colour)
{
	// { size.x * 1.2f, size.y }
	m_isKinematic = true;
	m_shapeID = TERRAIN;
	m_previous = previous;

}

void Terrain::draw()
{
	//Box::draw();

	glm::vec2 p1 = { 0,0 };
	glm::vec2 p2 = { 0,0 };
	glm::vec2 p3 = m_position - m_localX * m_extents.x + m_localY * m_extents.y;
	glm::vec2 p4 = m_position + m_localX * m_extents.x + m_localY * m_extents.y;

	if (m_previous != nullptr)
		p3 = m_previous->getP1();
	p1 = { p3.x, 0 };
	p2 = { p4.x, 0 };
	aie::Gizmos::add2DTri(p1, p2, p4, m_colour);
	aie::Gizmos::add2DTri(p1, p4, p3, m_colour);
	//aie::Gizmos::add2DAABB(m_position, m_extents,  {1,1,1,1}); // draw collision box
}

void Terrain::drawDebug()
{
	glm::vec2 p1 = m_position - m_localX * m_extents.x - m_localY * m_extents.y;
	glm::vec2 p2 = m_position + m_localX * m_extents.x - m_localY * m_extents.y;
	glm::vec2 p3 = m_position - m_localX * m_extents.x + m_localY * m_extents.y;
	glm::vec2 p4 = m_position + m_localX * m_extents.x + m_localY * m_extents.y;
	aie::Gizmos::add2DTri(p1, p2, p4, { 1,1,1,0.5f });
	aie::Gizmos::add2DTri(p1, p4, p3, { 1,1,1,0.5f });
}
