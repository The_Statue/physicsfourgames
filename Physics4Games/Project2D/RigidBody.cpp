#include "RigidBody.h"
#include <iostream>
#include "PhysicsScene.h"


Rigidbody::Rigidbody(ShapeType shapeID, glm::vec2 position, glm::vec2 velocity, float orientation, float mass) : PhysicsObject(shapeID)
{
	m_shapeID = shapeID;
	m_position = position;
	m_velocity = velocity;
	m_mass = mass;
	m_orientation = orientation;
	m_angularVelocity = 0;
	m_moment = 0;
	m_isKinematic = false;
	m_savedGravity = { 0,0 };

	m_initalPosition = m_position;
	m_initalVelocity = m_velocity;
	m_initalOrientation = m_orientation;
	m_initalAngularVelocity = m_angularVelocity;
}

Rigidbody::~Rigidbody()
{
}

void Rigidbody::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	if (m_isKinematic)
	{
		m_velocity = glm::vec2(0);
		m_angularVelocity = 0;
		return;
	}

	m_position += m_velocity * timeStep;
	m_savedGravity = gravity;

	//m_velocity += (gravity * m_mass * timeStep) / getMass();
	applyForce(gravity * getMass() * timeStep, { 0,0 }); //cant use apply force for gravity due to no position?
	m_orientation += m_angularVelocity * timeStep;


	m_velocity -= m_velocity * m_linearDrag * timeStep; //apply drag
	m_angularVelocity -= m_angularVelocity * m_angularDrag * timeStep;

	if (length(m_velocity) < MIN_LINEAR_THRESHOLD) {
		if (length(m_velocity) < length(gravity) * m_linearDrag * timeStep)
			m_velocity = glm::vec2(0, 0);
	}
	if (abs(m_angularVelocity) < MIN_ANGULAR_THRESHOLD) {
		m_angularVelocity = 0;
	}
}

void Rigidbody::applyForce(glm::vec2 force, glm::vec2 pos)
{
	//std::cout << id << "[" << force.x << " " << force.y << "]" << std::endl;
	m_velocity += force / getMass();
	m_angularVelocity += (force.y * pos.x - force.x * pos.y) / getMoment();
}

float Rigidbody::getKineticEnergy()
{
	return 0.5f * (getMass() * glm::dot(m_velocity, m_velocity) +
		getMoment() * m_angularVelocity * m_angularVelocity);
}

float Rigidbody::getPotentialEnergy()
{
	return -getMass() * glm::dot(m_savedGravity, getPosition());
}

float Rigidbody::getEnergy()
{
	return getKineticEnergy() + getPotentialEnergy();
}

glm::vec2 Rigidbody::toWorld(glm::vec2 localPos)
{
	return getPosition() + rotateAroundPoint(localPos, getOrientatation());
}

glm::vec2 Rigidbody::rotateAroundPoint(glm::vec2 localPos, float angle)
{
	glm::vec2 relativePos = { (localPos.x * cos(angle)) - (localPos.y * sin(angle)), (localPos.y * cos(angle)) + (localPos.x * sin(angle)) };
	return relativePos;
}

void Rigidbody::resetPosition()
{
	m_position = m_initalPosition;
	m_velocity = m_initalVelocity;
	m_orientation = m_initalOrientation;
	m_angularVelocity = m_initalAngularVelocity;
}

void Rigidbody::drawDebug()
{
}

void Rigidbody::resolveCollision(Rigidbody* actor2, glm::vec2 contact, glm::vec2* collisionNormal, float pen)
{
	// find the vector between their centres, or use the provided direction 
	// of force, and make sure it's normalised 
	glm::vec2 relativePosition = actor2->m_position - m_position;
	if (relativePosition.x == 0 && relativePosition.y == 0)
	{
		relativePosition = { 1,0 };
	}
	glm::vec2 normal = glm::normalize(collisionNormal ? *collisionNormal : relativePosition);
	// get the vector perpendicular to the collision normal 
	glm::vec2  perp(normal.y, -normal.x);

	// determine the total velocity of the contact points for the two objects,  
   // for both linear and rotational     

	 // 'r' is the radius from axis to application of force 
	float r1 = glm::dot(contact - m_position, -perp);
	float r2 = glm::dot(contact - actor2->m_position, perp);
	// velocity of the contact point on this object  
	float v1 = glm::dot(m_velocity, normal) - r1 * m_angularVelocity;
	// velocity of contact point on actor2 
	float v2 = glm::dot(actor2->m_velocity, normal) +
		r2 * actor2->m_angularVelocity;

	if (v1 > v2) // they're moving closer 
	{
		// calculate the effective mass at contact point for each object 
		// ie how much the contact point will move due to the force applied. 
		float mass1 = 1.0f / (1.0f / getMass() + (r1 * r1) / getMoment());
		float mass2 = 1.0f / (1.0f / actor2->getMass() + (r2 * r2) / actor2->getMoment());

		float elasticity = (getElasticity() + actor2->getElasticity()) / 2;

		glm::vec2 force = (1.0f + elasticity) * mass1 * mass2 /
			(mass1 + mass2) * (v1 - v2) * normal;

		float kePre1 = getEnergy();
		float kePre2 = actor2->getEnergy();


		//apply equal and opposite forces 
		applyForce(-force, contact - m_position);
		actor2->applyForce(force, contact - actor2->m_position);


		float kePost1 = getEnergy();
		float kePost2 = actor2->getEnergy();

		float deltaKE1 = kePost1 - kePre1;
		float deltaKE2 = kePost2 - kePre2;
		//if (deltaKE1 > kePost1 * 0.01f)
		//	std::cout << m_shapeID << " Kinetic Energy discrepancy greater than 1% detected!! [" << deltaKE1 << ", " << kePost2 * 0.01f << "]" << std::endl;
		//if (deltaKE2 > kePost2 * 0.01f)
		//	std::cout << m_shapeID << " Kinetic Energy discrepancy greater than 1% detected!! [" << deltaKE1 << ", " << kePost2 * 0.01f << "]" << std::endl;
		
		if (actor2->getShapeID() == SMOKE)
		{
			Smoke* smoke = dynamic_cast<Smoke*>(actor2);
			smoke->increaseCollisions();
		}
		if (this->getShapeID() == SMOKE)
		{
			Smoke* smoke = dynamic_cast<Smoke*>(this);
			smoke->increaseCollisions();
		}
	}
	if (pen > 0)
		PhysicsScene::ApplyContactForces(this, actor2, normal, pen);
}