#include "Application2D.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <Gizmos.h>
#include "glm\ext.hpp"
#include "SoftBody.h"

Application2D::Application2D() {

}

Application2D::~Application2D() {

}

bool Application2D::startup() {
	// increase the 2d line count to maximize the number of objects we can draw 
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);

	m_2dRenderer = new aie::Renderer2D();

	m_font = new aie::Font("./font/consolas.ttf", 32);
	m_bigFont = new aie::Font("./font/consolas.ttf", 64);

	m_timer = 0;

	m_physicsScene = new PhysicsScene();
	m_physicsScene->setGravity({ 0,-9.81f }); //-1.62f moon gravity (not as good?)
	m_physicsScene->setTimeStep(0.01); //smaller number means less energy inserted into system each frame

	//std::vector<std::string> sb;
	//sb.push_back("000000");
	//sb.push_back("000000");
	//sb.push_back("00....");
	//sb.push_back("00....");
	//sb.push_back("000000");
	//sb.push_back("000000");

	//SoftBody::Build(m_physicsScene, glm::vec2(-50, 50), 5.0f, 50.0f, 1, sb);

	generateTerrain();

	Box* ship = new Box({ -10, 2000 }, { -40, -100 }, 40.0f, 0, { 4,3 }, randColor());

	m_physicsScene->addActor(ship, 3);

	m_thrusterLeft = new Thruster(ship, 400 * m_physicsScene->getTimeStep(), 50, aie::INPUT_KEY_A, { -ship->getWidth() / 2,-2 });
	m_thrusterRight = new Thruster(ship, 400 * m_physicsScene->getTimeStep(), 50, aie::INPUT_KEY_D, { ship->getWidth() / 2,-2 });
	m_physicsScene->addActor(m_thrusterLeft, 0);
	m_physicsScene->addActor(m_thrusterRight, 0);

	m_cameraTarget = ship; //define the box as the target for following with camera

	return true;
}

void Application2D::shutdown() {

	delete m_font;
	delete m_bigFont;
	delete m_2dRenderer;
}

glm::vec4 Application2D::randColor() {
	return { ((rand() % 1001) / 1000.0f), ((rand() % 1001) / 1000.0f), ((rand() % 1001) / 1000.0f), 1 };
}

bool Application2D::drawBox(glm::vec2 size, glm::vec2 offset, glm::vec4 color, glm::vec2 mousePos = { -1,-1 }) {

	glm::vec2 Pos = { 0,1 * (extents / aspectRatio) };
	Pos += offset;
	glm::vec2 p1 = Pos + glm::vec2(-size.x, size.y);
	glm::vec2 p2 = Pos + glm::vec2(-size.x, -size.y);
	glm::vec2 p3 = Pos + glm::vec2(size.x, size.y);
	glm::vec2 p4 = Pos + glm::vec2(size.x, -size.y);

	if (mousePos.x >= p1.x && mousePos.y <= p1.y && mousePos.x <= p4.x && mousePos.y >= p4.y)
	{
		aie::Input* input = aie::Input::getInstance();
		if (input->isMouseButtonDown(0))
		{
			return true;
		}
		color.a = 0.5f;
	}
	aie::Gizmos::add2DTri(p1, p2, p4, color);
	aie::Gizmos::add2DTri(p1, p4, p3, color);
	return false;
}

void Application2D::update(float deltaTime) {

	m_timer += deltaTime;

	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	int xScreen, yScreen;
	switch (m_state)
	{
	case 0://main menu
		//draw white background
		drawBox({ extents, (extents / aspectRatio) }, { 0,0 }, { 1,1,1,1 });


		input->getMouseXY(&xScreen, &yScreen);
		xScreen = (xScreen * 2 * extents / getWindowWidth()) - extents;
		yScreen = (yScreen * 2 * extents / (aspectRatio * getWindowHeight()));

		//draw Title
		drawBox({ 15, 5 }, { 0,0.8 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen });

		//draw menu buttons
		if (drawBox({ 15, 5 }, { 0,0.3 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen }))
		{
			m_state = 2;
			camHeight = m_cameraTarget->getPosition().y;
		}
		if (drawBox({ 15, 5 }, { 0,-0.1 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen }))
		{
			m_state = 1;
		}
		if (drawBox({ 15, 5 }, { 0,-0.5 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen }))
		{
			quit();
		}

		//aie::Gizmos::add2DCircle({ xScreen, yScreen }, 2, 32, glm::vec4(0, 0, 1, 1));
		break;
	case 1://?		//draw white background
		drawBox({ extents, (extents / aspectRatio) }, { 0,0 }, { 1,1,1,1 });


		input->getMouseXY(&xScreen, &yScreen);
		xScreen = (xScreen * 2 * extents / getWindowWidth()) - extents;
		yScreen = (yScreen * 2 * extents / (aspectRatio * getWindowHeight()));

		//draw Title
		drawBox({ 22, 5 }, { 0,0.8 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen });


		//draw menu buttons
		if (drawBox({ 5, 5 }, { -0.06f * extents,0.3 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen }))
		{
		}
		if (drawBox({ 5, 5 }, { 0.06f * extents,0.3 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen }))
		{
			m_state = -1;
		}
		if (drawBox({ 15, 5 }, { 0,-0.1 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen }))
		{
			reset();
		}
		if (drawBox({ 15, 5 }, { 0,-0.5 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen }))
		{
			m_physicsScene->m_debugDraw = !m_physicsScene->m_debugDraw;
		}
		if (drawBox({ 15, 5 }, { -0.75 * extents,-0.75 * (extents / aspectRatio) }, { 0,0,0,1 }, { xScreen, yScreen }))
		{
			m_state = 0;
		}
		break;
	case 2://game
	{
		//if last frame the thrusters were active, create smoke particle this frame

		m_physicsScene->update(deltaTime);
		m_physicsScene->draw();

		if (m_thrusterLeft->m_triggered)
		{
			int detail = 8; //how many positions the random can choose from
			float deviation = 0.2; //larger number allows it to rotate more from directly down
			float flicker = ((float)(rand() % detail) / ((detail - 1) / deviation)) - (((detail - 1) / ((detail - 1) / deviation)) / 2.0f);
			float orientation = flicker + m_thrusterLeft->getOrientatation();

			Smoke* smoke = new Smoke(m_thrusterLeft->getPosition() + (m_thrusterLeft->rotateAroundPoint({ -0,-1 }, orientation) * 5.0f), m_thrusterLeft->getVelocity() + (m_thrusterLeft->rotateAroundPoint({ 0,-1 }, orientation) * 40.0f));
			m_physicsScene->addActor(smoke, 4);

			m_smokeArray[m_smokeIndex] = smoke;
			m_smokeIndex++;
			if (m_smokeIndex >= m_maxSmoke)
				m_smokeIndex = 0;

			m_physicsScene->removeActor(m_smokeArray[m_smokeIndex]);
			//delete m_smokeArray[m_smokeIndex];
		}
		if (m_thrusterRight->m_triggered)
		{
			int detail = 8; //how many positions the random can choose from
			float deviation = 0.2; //larger number allows it to rotate more from directly down
			float flicker = ((float)(rand() % detail) / ((detail - 1) / deviation)) - (((detail - 1) / ((detail - 1) / deviation)) / 2.0f);
			float orientation = flicker + m_thrusterRight->getOrientatation();

			Smoke* smoke = new Smoke(m_thrusterRight->getPosition() + (m_thrusterRight->rotateAroundPoint({ 0,-1 }, orientation) * 5.0f), m_thrusterRight->getVelocity() + (m_thrusterRight->rotateAroundPoint({ 0,-1 }, orientation) * 40.0f));
			m_physicsScene->addActor(smoke, 4);

			m_smokeArray[m_smokeIndex] = smoke;
			m_smokeIndex++;
			if (m_smokeIndex >= m_maxSmoke)
				m_smokeIndex = 0;

			m_physicsScene->removeActor(m_smokeArray[m_smokeIndex]);
			//delete m_smokeArray[m_smokeIndex];
		}

		if (input->wasKeyPressed(aie::INPUT_KEY_R))
			reset();

		if (input->isMouseButtonDown(0))
		{
			int xScreen, yScreen;
			input->getMouseXY(&xScreen, &yScreen);
			glm::vec2 worldPos = screenToWorld(glm::vec2(xScreen, yScreen));

			aie::Gizmos::add2DCircle(worldPos, 5, 32, glm::vec4(0, 0, 1, 1));
		}


		//Calculate all camera related zooming and positioning
		camPos = m_cameraTarget->getPosition();

		float heightDiff = 0;
		float targetCamHeight = 0;
		int count = 0;
		for (Terrain* chunck : m_terrainArray)
		{
			if (abs(chunck->getPosition().x - camPos.x) < 50)
			{
				targetCamHeight += chunck->getPosition().y + 10;
				heightDiff += camPos.y - chunck->getPosition().y + 50;
				count++;
			}
		}
		if (count > 0)
			targetCamHeight /= count; //get average height of nearby terrain
		if (count > 0)
			heightDiff /= count; //get average height of nearby terrain

		targetCamHeight *= 0.95f;
		heightDiff *= 1;
		//extents = +300; //1 * targetCamHeight

		if (camHeight < targetCamHeight)
			camHeight += ((targetCamHeight - camHeight) * deltaTime) * 2;
		if (camHeight > targetCamHeight)
			camHeight -= ((camHeight - targetCamHeight) * deltaTime) * 2;
		if (extents < heightDiff)
			extents += ((heightDiff - extents) * deltaTime) * 2;
		if (extents > heightDiff)
			extents -= ((extents - heightDiff) * deltaTime) * 2;

		if (heightDiff < 1 || heightDiff > 500)
		{
			extents = 1500;
			camHeight = 0;
			camPos.x = -100;
		}

		break;
	}
	default:
		break;
	}
	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
	{
		camPos = { 0,0 };
		extents = 100;
		camHeight = 0;

		m_state = 0;
	}
}

void Application2D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here! 
	aie::Gizmos::draw2D(glm::ortho<float>(-extents + camPos.x, extents + camPos.x, camHeight, 2 * (extents / aspectRatio) + camHeight, -1.0f, 1.0f));

	switch (m_state) //draw scene specific stuff
	{
	case 0://main menu
		m_2dRenderer->drawText(m_bigFont, "Game!", getWindowWidth() / 2.3, getWindowHeight() / 1.15);

		m_2dRenderer->drawText(m_font, "Play!", getWindowWidth() / 2.15, getWindowHeight() / 1.57);
		m_2dRenderer->drawText(m_font, "Options!", getWindowWidth() / 2.23, getWindowHeight() / 2.3);
		m_2dRenderer->drawText(m_font, "Exit!", getWindowWidth() / 2.15, getWindowHeight() / 4.2);
		break;
	case 1://options
		m_2dRenderer->drawText(m_bigFont, "Options!", getWindowWidth() / 2.53, getWindowHeight() / 1.14);

		m_2dRenderer->drawText(m_font, "+", getWindowWidth() / 1.91, getWindowHeight() / 1.57);
		m_2dRenderer->drawText(m_font, "-", getWindowWidth() / 2.15, getWindowHeight() / 1.57);
		m_2dRenderer->drawText(m_font, "Regen!", getWindowWidth() / 2.16, getWindowHeight() / 2.3);
		m_2dRenderer->drawText(m_font, "DebugDraw", getWindowWidth() / 2.15, getWindowHeight() / 4.2);
		m_2dRenderer->drawText(m_font, "Back!", getWindowWidth() / 10.5, getWindowHeight() / 9);
		break;
	case 2://game
		// output some text, uses the last used colour
		char fps[32];
		char velocityUI[64];
		sprintf_s(fps, 32, "FPS: %i", getFPS());
		sprintf_s(velocityUI, 64, "Velocity: {%i,%i}", (int)m_cameraTarget->getVelocity().x, (int)m_cameraTarget->getVelocity().y);
		m_2dRenderer->drawText(m_font, fps, 0, 720 - 32);
		m_2dRenderer->drawText(m_font, "Press ESC to quit!", 0, 720 - 64);
		m_2dRenderer->drawText(m_font, velocityUI, 0, 720 - 96);
		break;
	default:
		m_2dRenderer->drawText(m_font, "An error has occured!", getWindowWidth() / 3, getWindowHeight() / 2);
		m_2dRenderer->drawText(m_font, "Press esc, it may fix it?", getWindowWidth() / 3, getWindowHeight() / 2.2);
		break;
	}
	// done drawing sprites
	m_2dRenderer->end();
}

glm::vec2 Application2D::screenToWorld(glm::vec2 screenPos)
{
	glm::vec2 worldPos = screenPos;

	// move the centre of the screen to (0,0) 
	worldPos.x -= getWindowWidth() / 2;
	worldPos.y -= getWindowHeight() / 2;

	// scale according to our extents 
	worldPos.x *= 2.0f * extents / getWindowWidth();
	worldPos.y *= 2.0f * extents / (aspectRatio * getWindowHeight());

	//adjust for zooming out
	worldPos.y -= m_cameraTarget->getPosition().y - (extents / aspectRatio);


	worldPos += camPos;

	return worldPos;
}
glm::vec2 Application2D::worldToScreen(glm::vec2 worldPos)
{
	glm::vec2 screenPos = worldPos;

	// move the centre of the screen to (0,0) 
	screenPos.x += getWindowWidth() / 2;
	screenPos.y += getWindowHeight() / 2;

	// scale according to our extents 
	screenPos.x /= 2.0f * extents / getWindowWidth();
	screenPos.y /= 2.0f * extents / (aspectRatio * getWindowHeight());

	float camPosX;
	float camPosY;
	m_2dRenderer->getCameraPos(camPosX, camPosY);
	screenPos += glm::vec2(camPosX * 6.5, camPosY * 6.5);
	//unused
	return screenPos;
}

void Application2D::generateTerrain()
{
	//ensure terrain is removed so you only have one
	if (generatedTerrain)
	{
		for (int i = 0; i < m_maxTerrain; i++)
		{
			m_physicsScene->removeActor(m_terrainArray[i]);
			delete m_terrainArray[i];
		}
	}

	float variance = 0;
	float width = 5.0f;
	float height = 1.0f;
	float orientation = 0.1f;
	glm::vec2 previousS = { -1000,rand() % 750 + 50 };
	Terrain* previous = nullptr;
	const double maxAngle = 3.14159265358979323846 / 2.5f;

	bool aboveZero = true;

	for (int i = 0; i < m_maxTerrain; i++)
	{
		glm::vec2 pos = { -1,-1 };;

		//ensure it will not dip below 0 by regenerating angle until it is above
		while (pos.y < 0)
		{
			variance = ((rand() % 101) - 50) * 0.01f;

			orientation += variance;

			if (orientation > maxAngle)
				orientation = maxAngle;
			if (orientation < -maxAngle)
				orientation = -maxAngle;

			pos = glm::vec2((width * cos(orientation)) - (-height * sin(orientation)), (-height * cos(orientation)) + (width * sin(orientation))) + previousS;
		}

		m_terrainArray[i] = new Terrain(pos, orientation, previous, { width,height }, { ((rand() % 101) / 1000.0f) + 0.7f, 0.3f, 0.1f, 1 });
		m_physicsScene->addActor(m_terrainArray[i], 5);
		m_terrainArray[i]->m_isKinematic = true;
		previous = m_terrainArray[i];

		previousS = pos;

		//apply rotation to get next piece to target the end of the block, not the center
		previousS = glm::vec2((width * cos(orientation)) - (height * sin(orientation)), (height * cos(orientation)) + (width * sin(orientation))) + previousS;

	}
	generatedTerrain = true;
}

void Application2D::reset()
{
	generateTerrain();
	m_physicsScene->resetPositions();//add reset button
}