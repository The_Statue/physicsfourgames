#include "PhysicsScene.h"

PhysicsScene::PhysicsScene() {
	m_timeStep = 0.01f;
	m_gravity = { 0, 0 };

	m_layers.resize(6);

	//m_initialFrameEnergy = getTotalEnergy();
}

PhysicsScene::~PhysicsScene() {
	for (auto pLayer : m_layers)
	{
		for (auto pActor : pLayer.m_actors)
		{
			delete pActor;
		}
	}
}

void PhysicsScene::addActor(PhysicsObject* actor, int layer)
{
	m_layers[layer].m_actors.push_back(actor);
}

void PhysicsScene::removeActor(PhysicsObject* actor)
{
	for (int j = 0; j < m_layers.size(); j++)
	{
		for (int i = 0; i < m_layers[j].m_actors.size(); i++)
		{
			if (m_layers[j].m_actors[i] == actor)
			{
				m_layers[j].m_actors.erase(m_layers[j].m_actors.begin() + i);
				break;
			}
		}
	}
}

// function pointer array for doing our collisions 
typedef bool(*fn)(PhysicsObject*, PhysicsObject*);

static fn collisionFunctionArray[] =
{
 PhysicsScene::plane2Plane,		PhysicsScene::plane2Sphere,		PhysicsScene::plane2Box,	PhysicsScene::plane2Smoke,		PhysicsScene::plane2Terrain,
 PhysicsScene::sphere2Plane,	PhysicsScene::sphere2Sphere,	PhysicsScene::sphere2Box,	PhysicsScene::sphere2Smoke,		PhysicsScene::sphere2Terrain,
 PhysicsScene::box2Plane,		PhysicsScene::box2Sphere,		PhysicsScene::box2Box,		PhysicsScene::box2Smoke,		PhysicsScene::box2Terrain,
 PhysicsScene::smoke2Plane,		PhysicsScene::smoke2Sphere,		PhysicsScene::smoke2Box,	PhysicsScene::smoke2Box,		PhysicsScene::smoke2Terrain,
 PhysicsScene::terrain2Plane,	PhysicsScene::terrain2Sphere,	PhysicsScene::terrain2Box,	PhysicsScene::terrain2Smoke,	PhysicsScene::terrain2Terrain
};

void PhysicsScene::update(float dt)
{
	// update physics at a fixed time step 

	static float accumulatedTime = 0.0f;
	accumulatedTime += dt;

	while (accumulatedTime >= m_timeStep)
	{
		for (auto pLayer : m_layers)
		{
			for (auto pActor : pLayer.m_actors)
			{
				pActor->fixedUpdate(m_gravity, m_timeStep);
				if (pActor->getMarkDelete())
					removeActor(pActor);
			}
		}

		accumulatedTime -= m_timeStep;

		// check for collisions (ideally you'd want to have some sort of  
		// scene management in place) 
		//int actorCount = m_actors.size();


		for (int iLayer = 0; iLayer < m_layers.size(); iLayer++)
		{
			for (int jLayer = iLayer; jLayer < m_layers.size(); jLayer++)
			{
				if (/*Ensure joints dont collide*/(iLayer > 0 && jLayer > 0) && /*Ensure planes layers dont collide*/!(iLayer == 1 && jLayer == 1) && /*Ensure smoke Layers dont collide*/!(iLayer == 4 && jLayer == 4) && /*Ensure terrain Layers dont collide*/!(iLayer == 5 && jLayer == 5) && /*Ensure terrain and plane Layers dont collide*/!((iLayer == 1 && jLayer == 5) || (iLayer == 5 && jLayer == 5))) // do these layers actually collide?
				{
					for (int iObj = 0; iObj < m_layers[iLayer].m_actors.size(); iObj++)
					{
						int startIndex = (iLayer == jLayer) ? iObj + 1 : 0;
						for (int jObj = startIndex; jObj < m_layers[jLayer].m_actors.size(); jObj++)
						{
							PhysicsObject* obj1 = m_layers[iLayer].m_actors[iObj];
							PhysicsObject* obj2 = m_layers[jLayer].m_actors[jObj];

							// do collision stuff
							AllocateCollision(obj1, obj2);
						}
					}
				}
			}
		}

		// need to check for collisions against all objects except this one. 
		//for (int outer = 0; outer < actorCount - 1; outer++)
		//{
		//	PhysicsObject* object1 = m_actors[outer];
		//	int shapeId1 = object1->getShapeID();
		//	int loopCount = 0;

		//	for (int inner = outer + 1; inner < actorCount; inner++)
		//	{
		//		PhysicsObject* object2 = m_actors[inner];
		//		AllocateCollision(object1, object2);
		//	}
		//}
	}
	float energy = getTotalEnergy();

	if (m_initialFrameEnergy <= 0 || m_initialFrameEnergy >= 1000000000)
		m_initialFrameEnergy = energy;

	if ((energy < m_lowestFrameEnergy && energy > 0) || m_lowestFrameEnergy < 0)
		m_lowestFrameEnergy = energy;
	if (energy > m_highestFrameEnergy && energy < 1000000000)
		m_highestFrameEnergy = energy;

	//std::cout << "inital[" << m_initialFrameEnergy << "]\tlowest[" << m_lowestFrameEnergy << "]\thighest[" << m_highestFrameEnergy << "]\t[" << energy - m_initialFrameEnergy << "]\t[" << energy - m_previousFrameEnergy << "]\t[" << energy << "]" << std::endl;
	m_previousFrameEnergy = energy;
}

void PhysicsScene::AllocateCollision(PhysicsObject* object1, PhysicsObject* object2)
{
	int shapeId1 = object1->getShapeID();
	int shapeId2 = object2->getShapeID();

	if (!(glm::distance(object1->getPosition(), object2->getPosition()) < 15 || (shapeId1 == 0 || shapeId2 == 0)))
		return;

	// using function pointers 
	int functionIdx = (shapeId1 * SHAPE_COUNT) + shapeId2;
	fn collisionFunctionPtr = collisionFunctionArray[functionIdx];
	if (collisionFunctionPtr != nullptr)
	{
		// did a collision occur? 
		collisionFunctionPtr(object1, object2);
	}
}

void PhysicsScene::draw()
{
	for (auto pLayer : m_layers)
	{
		for (int i = pLayer.m_actors.size(); i-- > 0; )
		{
			//for (auto pActor : m_actors) {
			pLayer.m_actors[i]->draw();
		}
	}
	if (m_debugDraw)
	{
		for (auto pLayer : m_layers)
		{
			for (auto pActor : pLayer.m_actors) {
				pActor->drawDebug();
			}
		}
	}
}

void PhysicsScene::resetPositions()
{
	for (auto pLayer : m_layers)
	{
		for (auto pActor : pLayer.m_actors) {
			pActor->resetPosition();
		}
	}
}

float PhysicsScene::getTotalEnergy()
{
	float total = 0;
	for (auto pLayer : m_layers)
	{
		for (auto it = pLayer.m_actors.begin(); it != pLayer.m_actors.end(); it++)
		{
			PhysicsObject* obj = *it;
			total += obj->getEnergy();
		}
	}
	return total;
}




bool PhysicsScene::plane2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return false;
}

bool PhysicsScene::plane2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	// reverse the order of arguments, as obj1 is the plane and obj2 is the sphere 
	return sphere2Plane(obj2, obj1);
}

bool PhysicsScene::plane2Box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Plane* plane = dynamic_cast<Plane*>(obj1);
	Box* box = dynamic_cast<Box*>(obj2);

	//if we are successful then test for collision 
	if (box != nullptr && plane != nullptr)
	{
		int numContacts = 0;
		glm::vec2 contact(0, 0);
		float contactV = 0;

		// get a representative point on the plane 
		glm::vec2 planeOrigin = plane->getNormal() * plane->getDistance();

		// check all four corners to see if we've hit the plane 
		for (float x = -box->getExtents().x; x < box->getWidth(); x += box->getWidth())
		{
			for (float y = -box->getExtents().y; y < box->getHeight(); y += box->getHeight())
			{
				// get the position of the corner in world space 
				glm::vec2 p = box->getPosition() + x * box->getLocalX() + y * box->getLocalY();
				float distFromPlane = glm::dot(p - planeOrigin, plane->getNormal());

				// this is the total velocity of the point in world space 
				glm::vec2 displacement = x * box->getLocalX() + y * box->getLocalY();
				glm::vec2 pointVelocity = box->getVelocity() + box->getAngularVelocity() * glm::vec2(-displacement.y, displacement.x);
				// and this is the component of the point velocity into the plane 
				float velocityIntoPlane = glm::dot(pointVelocity, plane->getNormal());

				// and moving further in, we need to resolve the collision 
				if (distFromPlane < 0 && velocityIntoPlane <= 0)
				{
					numContacts++;
					contact += p;
					contactV += velocityIntoPlane;
				}
			}
		}

		// we've had a hit - typically only two corners can contact 
		if (numContacts > 0)
		{
			plane->resolveCollision(box, contact / (float)numContacts);
			return true;
		}
	}

	return false;
}

bool PhysicsScene::plane2Smoke(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return smoke2Plane(obj2, obj1);
}

bool PhysicsScene::plane2Terrain(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return false;
}




bool PhysicsScene::sphere2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Sphere* sphere = dynamic_cast<Sphere*>(obj1);
	Plane* plane = dynamic_cast<Plane*>(obj2);
	//if we are successful then test for collision 
	if (sphere != nullptr && plane != nullptr)
	{
		glm::vec2 collisionNormal = plane->getNormal();
		float sphereToPlane = glm::dot(sphere->getPosition(), plane->getNormal()) - plane->getDistance();

		float intersection = sphere->getRadius() - sphereToPlane;
		float velocityOutOfPlane = glm::dot(sphere->getVelocity(), plane->getNormal());
		if (intersection > 0 && velocityOutOfPlane < 0)
		{
			glm::vec2 contact = sphere->getPosition() + (collisionNormal * -sphere->getRadius());
			plane->resolveCollision(sphere, contact);
			//sphere->applyForce(-(sphere->getVelocity() * sphere->getMass()));
			return true;
		}
	}
	return false;
}

bool PhysicsScene::sphere2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Sphere* sphere1 = dynamic_cast<Sphere*>(obj1);
	Sphere* sphere2 = dynamic_cast<Sphere*>(obj2);
	if (sphere1 != nullptr && sphere2 != nullptr) {
		float dist = glm::distance(sphere1->getPosition(), sphere2->getPosition());
		float penetration = sphere1->getRadius() + sphere2->getRadius() - dist;
		if (penetration > 0)
		{
			sphere1->resolveCollision(sphere2, (sphere1->getPosition() + sphere2->getPosition()) * 0.5f, nullptr, penetration);
			return true;
		}
	}
	return false;
}

bool PhysicsScene::sphere2Box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return box2Sphere(obj2, obj1);
}

bool PhysicsScene::sphere2Smoke(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return smoke2Sphere(obj2, obj1);
}

bool PhysicsScene::sphere2Terrain(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return sphere2Box(obj2, obj1);
}




bool PhysicsScene::box2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	// reverse the order of arguments
	return plane2Box(obj2, obj1);
}

bool PhysicsScene::box2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Box* box = dynamic_cast<Box*>(obj1);
	Sphere* sphere = dynamic_cast<Sphere*>(obj2);

	if (box != nullptr && sphere != nullptr)
	{
		// transform the circle into the box's coordinate space 
		glm::vec2 circlePosWorld = sphere->getPosition() - box->getPosition();
		glm::vec2 circlePosBox = glm::vec2(glm::dot(circlePosWorld, box->getLocalX()), glm::dot(circlePosWorld, box->getLocalY()));

		// find the closest point to the circle centre on the box by clamping the coordinates in box - space to the box's extents 
		glm::vec2 closestPointOnBoxBox = circlePosBox;
		glm::vec2 extents = box->getExtents();
		if (closestPointOnBoxBox.x < -extents.x) closestPointOnBoxBox.x = -extents.x;
		if (closestPointOnBoxBox.x > extents.x) closestPointOnBoxBox.x = extents.x;
		if (closestPointOnBoxBox.y < -extents.y) closestPointOnBoxBox.y = -extents.y;
		if (closestPointOnBoxBox.y > extents.y) closestPointOnBoxBox.y = extents.y;
		// and convert back into world coordinates 
		glm::vec2 closestPointOnBoxWorld = box->getPosition() + closestPointOnBoxBox.x * box->getLocalX() + closestPointOnBoxBox.y * box->getLocalY();
		glm::vec2 circleToBox = sphere->getPosition() - closestPointOnBoxWorld;

		if (circleToBox.x == 0 && circleToBox.y == 0)
		{
			circleToBox = { 1,0 };
		}

		float penetration = sphere->getRadius() - glm::length(circleToBox);
		if (penetration > 0)
		{
			glm::vec2 direction = glm::normalize(circleToBox);
			glm::vec2 contact = closestPointOnBoxWorld;
			box->resolveCollision(sphere, contact, &direction, penetration);
		}
	}

	return false;
}

bool PhysicsScene::box2Box(PhysicsObject* obj1, PhysicsObject* obj2) {
	Box* box1 = dynamic_cast<Box*>(obj1);
	Box* box2 = dynamic_cast<Box*>(obj2);
	if (box1 != nullptr && box2 != nullptr) {
		glm::vec2 boxPos = box2->getPosition() - box1->getPosition();
		glm::vec2 norm(0, 0);
		glm::vec2 contact(0, 0);
		float pen = 0;
		int numContacts = 0;
		box1->checkBoxCorners(*box2, contact, numContacts, pen, norm);
		if (box2->checkBoxCorners(*box1, contact, numContacts, pen, norm)) {
			norm = -norm;
		}
		if (pen > 0) {
			box1->resolveCollision(box2, contact / float(numContacts), &norm, pen);
		}
		return true;
	}
	return false;
}

bool PhysicsScene::box2Smoke(PhysicsObject* obj1, PhysicsObject* obj2) {

	return smoke2Box(obj2, obj1);
}

bool PhysicsScene::box2Terrain(PhysicsObject* obj1, PhysicsObject* obj2) {

	return box2Box(obj2, obj1);
}




bool PhysicsScene::smoke2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return sphere2Plane(obj1, obj2);
}

bool PhysicsScene::smoke2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return sphere2Sphere(obj1, obj2);
}

bool PhysicsScene::smoke2Box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return sphere2Box(obj1, obj2);
}

bool PhysicsScene::smoke2Smoke(PhysicsObject* obj1, PhysicsObject* obj2) {

	return false;
}

bool PhysicsScene::smoke2Terrain(PhysicsObject* obj1, PhysicsObject* obj2) {

	return  sphere2Box(obj1, obj2);
}




bool PhysicsScene::terrain2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return false;
}

bool PhysicsScene::terrain2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return box2Sphere(obj1, obj2);
}

bool PhysicsScene::terrain2Box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return box2Box(obj1, obj2);
}

bool PhysicsScene::terrain2Smoke(PhysicsObject* obj1, PhysicsObject* obj2) {

	return smoke2Box(obj2, obj1);
}

bool PhysicsScene::terrain2Terrain(PhysicsObject* obj1, PhysicsObject* obj2) {

	return false;
}




// body2 can be null for a Plane 
void PhysicsScene::ApplyContactForces(Rigidbody* body1, Rigidbody* body2, glm::vec2 norm, float pen)
{
	float body2Mass = body2 ? body2->getMass() : INT_MAX;

	float body1Factor = body2Mass / (body1->getMass() + body2Mass);

	body1->setPosition(body1->getPosition() - body1Factor * norm * pen);
	if (body2)
		body2->setPosition(body2->getPosition() + (1 - body1Factor) * norm * pen);
}