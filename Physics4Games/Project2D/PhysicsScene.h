#pragma once
#include "Sphere.h"
#include "Plane.h"
#include "Box.h"
#include "Spring.h"
#include "Thruster.h"
#include "Smoke.h"
#include "Terrain.h"
//#include "SoftBody.h"
#include <vector>

class PhysicsScene
{
public:
	PhysicsScene();
	~PhysicsScene();

	void addActor(PhysicsObject* actor, int layer);
	void removeActor(PhysicsObject* actor);
	void update(float dt);
	void AllocateCollision(PhysicsObject* object1, PhysicsObject* object2);
	void draw();
	void resetPositions();

	float getTotalEnergy();

	void setGravity(const glm::vec2 gravity) { m_gravity = gravity; }
	glm::vec2 getGravity() const { return m_gravity; }

	void setTimeStep(const float timeStep) { m_timeStep = timeStep; }
	float getTimeStep() const { return m_timeStep; }

	static bool plane2Plane(PhysicsObject*, PhysicsObject*);
	static bool plane2Sphere(PhysicsObject*, PhysicsObject*);
	static bool plane2Box(PhysicsObject*, PhysicsObject*);
	static bool plane2Smoke(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool plane2Terrain(PhysicsObject* obj1, PhysicsObject* obj2);

	static bool sphere2Plane(PhysicsObject*, PhysicsObject*);
	static bool sphere2Sphere(PhysicsObject*, PhysicsObject*);
	static bool sphere2Box(PhysicsObject*, PhysicsObject*);
	static bool sphere2Smoke(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool sphere2Terrain(PhysicsObject* obj1, PhysicsObject* obj2);

	static bool box2Plane(PhysicsObject*, PhysicsObject*);
	static bool box2Sphere(PhysicsObject*, PhysicsObject*);
	static bool box2Box(PhysicsObject*, PhysicsObject*);
	static bool box2Smoke(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool box2Terrain(PhysicsObject* obj1, PhysicsObject* obj2);

	static bool smoke2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool smoke2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool smoke2Box(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool smoke2Smoke(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool smoke2Terrain(PhysicsObject* obj1, PhysicsObject* obj2);

	static bool terrain2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool terrain2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool terrain2Box(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool terrain2Smoke(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool terrain2Terrain(PhysicsObject* obj1, PhysicsObject* obj2);

	static void ApplyContactForces(Rigidbody* body1, Rigidbody* body2, glm::vec2 norm, float pen);

	bool m_debugDraw = false;
protected:
	glm::vec2 m_gravity;
	float m_timeStep;
	float m_lowestFrameEnergy = 0;
	float m_highestFrameEnergy = 0;
	float m_initialFrameEnergy = 0;
	float m_previousFrameEnergy = 0;
	//std::vector<PhysicsObject*> m_actors;

	struct Layer
	{
		std::vector<PhysicsObject*> m_actors;
	};
	std::vector<Layer> m_layers;
};

