#include "Smoke.h"
#include <Gizmos.h>

Smoke::Smoke(glm::vec2 position, glm::vec2 velocity) : 
	Sphere(position, velocity, 0.01f, (rand() % 1 + 1), {0.5f,0.5f,0.5f,0.5f})
{
	m_shapeID = SMOKE;
}
void Smoke::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	m_timePass += timeStep;
	if (m_collisions >= m_maxCollisions || m_timePass >= m_maxTimePass)
		m_markDelete = true;

	Sphere::fixedUpdate({ 0,0.1f }, timeStep);
}
