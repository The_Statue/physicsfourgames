#include "Thruster.h"
#include "PhysicsScene.h"
#include <Gizmos.h>

Thruster::Thruster(Rigidbody* body, float thrustStrength, float fuel, aie::EInputCodes triggerButton, glm::vec2 contact) :
	Rigidbody(JOINT, { 0,0 }, { 0,0 }, 0, 0)
{
	m_body = body;
	m_contact = contact;

	m_thrustStrength = thrustStrength;
	m_fuel = fuel;
	m_initialFuel = fuel;
	m_triggerButton = triggerButton;
}

void Thruster::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	m_triggered = input->isKeyDown(m_triggerButton) && m_fuel > 0;

	if (m_triggered)
	{
		m_fuel -= m_fuelConsumption * timeStep;

		glm::vec2 force = { -1 * sin(getOrientatation()), 1 * cos(getOrientatation()) };
		force *= m_thrustStrength;
		m_body->applyForce(force, getLocalPosition());

	}

	//store the local axes 
	float cs = cosf(getOrientatation());
	float sn = sinf(getOrientatation());
	m_localX = glm::normalize(glm::vec2(cs, sn));
	m_localY = glm::normalize(glm::vec2(-sn, cs));
}

void Thruster::draw()
{
	if (m_triggered)
	{
		int detail = 8; //how many positions the random can choose from
		float deviation = 0.1; //larger number allows it to rotate more from directly down
		float flicker = ((float)(rand() % detail) / ((detail - 1) / deviation)) - (((detail - 1) / ((detail - 1) / deviation)) / 2.0f);

		//Draw red flame
		glm::vec2 p1 = getPosition() - rotateAroundPoint(m_localY * 10.0f, flicker);// +m_localX * flicker;
		glm::vec2 p3 = getPosition() - rotateAroundPoint(m_localX * 2.0f, flicker);
		glm::vec2 p4 = getPosition() + rotateAroundPoint(m_localX * 2.0f, flicker);
		//aie::Gizmos::add2DTri(p1, p2, p4, { 1,0,0,1 });
		aie::Gizmos::add2DTri(p1, p4, p3, { 1,0,0,1 });

		//Draw orange flame
		p1 = getPosition() - rotateAroundPoint(m_localY * 8.0f, flicker);// +m_localX * flicker;
		p3 = getPosition() - rotateAroundPoint(m_localX * 1.5f, flicker);
		p4 = getPosition() + rotateAroundPoint(m_localX * 1.5f, flicker);
		aie::Gizmos::add2DTri(p1, p4, p3, { 1,0.5f,0,1 });
	}
	aie::Gizmos::add2DCircle(getPosition(), 2, 12, { 1,1,1,1 });

	// draw using local axes (background box for fuel gauge)
	glm::vec2 p1 = getPosition() - m_localX * 0.5f;
	glm::vec2 p2 = getPosition() + m_localX * 0.5f;
	glm::vec2 p3 = getPosition() - m_localX * 0.5f + m_localY * 4.0f;
	glm::vec2 p4 = getPosition() + m_localX * 0.5f + m_localY * 4.0f;
	aie::Gizmos::add2DTri(p1, p2, p4, { 0,0,0,1 });
	aie::Gizmos::add2DTri(p1, p4, p3, { 0,0,0,1 });

	// draw using local axes (moving front box for fuel gauge)
	p1 = getPosition() - m_localX * 0.5f;
	p2 = getPosition() + m_localX * 0.5f;
	p3 = getPosition() - m_localX * 0.5f + m_localY * (m_fuel / (m_initialFuel / 4));
	p4 = getPosition() + m_localX * 0.5f + m_localY * (m_fuel / (m_initialFuel / 4));
	aie::Gizmos::add2DTri(p1, p2, p4, { 0,1,0,1 });
	aie::Gizmos::add2DTri(p1, p4, p3, { 0,1,0,1 });
}