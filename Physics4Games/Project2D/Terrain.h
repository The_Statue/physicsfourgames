#pragma once
#include "Box.h"

class Terrain : public Box
{
public:
	Terrain(glm::vec2 position, float orientation, Terrain* previous, glm::vec2 size, glm::vec4 colour);
	void draw();
	void drawDebug();
	glm::vec2 getP1() { return (m_position + m_localX * m_extents.x + m_localY * m_extents.y); }
protected:
	Terrain* m_previous;
};

