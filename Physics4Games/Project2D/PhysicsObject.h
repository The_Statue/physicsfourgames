#pragma once
#include "glm/glm.hpp"
#include <iostream>

enum ShapeType {
	JOINT = -1,
	PLANE = 0,
	SPHERE,
	BOX,
	SMOKE,
	TERRAIN,
	SHAPE_COUNT
};

class PhysicsObject
{
protected:
	PhysicsObject(ShapeType a_shapeID) : m_shapeID(a_shapeID) {}

public:
	virtual void fixedUpdate(glm::vec2 gravity, float timeStep) = 0;
	virtual void draw() = 0;
	virtual void drawDebug() = 0;
	virtual void resetPosition() {};
	virtual glm::vec2 getPosition() { return m_position; }
	virtual float getEnergy() { return 0; };
	virtual bool getMarkDelete() { return m_markDelete; };

	ShapeType getShapeID() { return m_shapeID; };

protected:
	ShapeType m_shapeID;
	glm::vec2 m_position;
	bool m_markDelete = false;
};