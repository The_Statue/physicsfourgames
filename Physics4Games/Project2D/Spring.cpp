#include "Spring.h"
#include <Gizmos.h>

Spring::Spring(Rigidbody* body1, Rigidbody* body2, float springCoefficient, float damping, float restLength, glm::vec2 contact1, glm::vec2 contact2) :
	Rigidbody(JOINT, { 0,0 }, { 0,0 }, 0, 0)
{
	m_body1 = body1;
	m_body2 = body2;

	m_contact1 = contact1;
	m_contact2 = contact2;

	m_restLength = restLength;
	m_springCoefficient = springCoefficient;
	m_damping = damping;

	if (m_restLength == 0)
	{
		m_restLength = glm::distance(m_body1->toWorld(m_contact1), m_body2->toWorld(m_contact2));
	}
}

void Spring::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	glm::vec2 p2 = m_body2->toWorld(m_contact2);
	glm::vec2 p1 = m_body1->toWorld(m_contact1);
	glm::vec2 dist = p2 - p1;
	float length = sqrtf(dist.x * dist.x + dist.y * dist.y);
	// apply damping
	glm::vec2 relativeVelocity = m_body2->getVelocity() - m_body1->getVelocity();
	// F = -kX - bv
	glm::vec2 force = dist * m_springCoefficient * (m_restLength - length) - m_damping * relativeVelocity;
	force *= timeStep;

	// cap the spring force to 1000 N to prevent numerical instability 
	const float threshold = 1000.0f;
	float forceMag = glm::length(force);
	if (forceMag > threshold)
		force *= threshold / forceMag;

	m_body1->applyForce(-force, { 0,0 });
	m_body2->applyForce(force, { 0,0 });
}

void Spring::draw()
{
	aie::Gizmos::add2DLine(m_body1->toWorld(m_contact1), m_body2->toWorld(m_contact2), { 1,1,1,1 });
}