#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "PhysicsScene.h"

class Application2D : public aie::Application {
public:

	Application2D();
	virtual ~Application2D();

	virtual bool startup();
	virtual void shutdown();

	glm::vec4 randColor();

	bool drawBox(glm::vec2 size, glm::vec2 offset, glm::vec4 color, glm::vec2 mousePos);

	virtual void update(float deltaTime);
	virtual void draw();

	glm::vec2 screenToWorld(glm::vec2 screenPos);
	glm::vec2 worldToScreen(glm::vec2 worldPos);

	void generateTerrain();
	void reset();

protected:

	aie::Renderer2D* m_2dRenderer;
	aie::Font* m_font;
	aie::Font* m_bigFont;

	Rigidbody* m_cameraTarget;
	Thruster* m_thrusterLeft;
	Thruster* m_thrusterRight;

	static const int m_maxTerrain = 300;
	Terrain* m_terrainArray[m_maxTerrain];
	bool generatedTerrain = false;

	static const int m_maxSmoke = 600;
	Smoke* m_smokeArray[m_maxSmoke];
	int m_smokeIndex = 0;

	glm::vec2 camPos = { 0,0 };
	//float targetCamHeight = 0;
	float camHeight = 0;
	//float targetCamExtents = 0;
	float extents = 100;
	const float aspectRatio = 16.0f / 9.0f;

	PhysicsScene* m_physicsScene;
	float m_timer;

	int m_state = 0; // 0 = mainmenu, 2 = game
};