#pragma once
#include "RigidBody.h"
#include <Input.h>

class Thruster : public Rigidbody
{
public:
	Thruster(Rigidbody* body, float thrustStrength, float fuel, aie::EInputCodes triggerButton, glm::vec2 contact = glm::vec2(0, 0));

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	virtual void draw();
	glm::vec2 getVelocity() { return m_body->getVelocity(); }
	glm::vec2 getPosition() { return m_body->getPosition() + getLocalPosition(); }
	glm::vec2 getLocalPosition() { return { (m_contact.x * cos(getOrientatation())) - (m_contact.y * sin(getOrientatation())), (m_contact.y * cos(getOrientatation())) + (m_contact.x * sin(getOrientatation())) }; }
	float getOrientatation() { return m_body->getOrientatation(); }

	void resetPosition() { m_fuel = m_initialFuel; }
	bool m_triggered = false;
protected:
	Rigidbody* m_body;
	glm::vec2 m_contact;

	float m_thrustStrength;
	float m_initialFuel;
	float m_fuel;
	float m_fuelConsumption = 1;


	glm::vec2 m_localX;
	glm::vec2 m_localY;

	aie::Input* input = aie::Input::getInstance();
	aie::EInputCodes m_triggerButton;
};