#pragma once
#include "Sphere.h"

class Smoke : public Sphere
{
public:
	Smoke(glm::vec2 position, glm::vec2 velocity);
	~Smoke();

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	void increaseCollisions() { m_collisions++; }
protected:
	int m_collisions = 0;
	static const int m_maxCollisions = 2;

	float m_timePass = 0;
	static const int m_maxTimePass = 3;
};