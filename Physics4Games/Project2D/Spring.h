#pragma once
#include "RigidBody.h"

class Spring : public Rigidbody
{
public:
	Spring(Rigidbody* body1, Rigidbody* body2, float springCoefficient, float damping = 0.1f, float restLength = 0.0f,
		glm::vec2 contact1 = glm::vec2(0, 0), glm::vec2 contact2 = glm::vec2(0, 0));

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	virtual void draw();

protected:
	Rigidbody* m_body1;
	Rigidbody* m_body2;

	glm::vec2 m_contact1;
	glm::vec2 m_contact2;

	float m_springCoefficient;
	float m_restLength;
	float m_damping;
};

